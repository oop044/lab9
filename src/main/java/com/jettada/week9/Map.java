package com.jettada.week9;

public class Map {
    private int width;
    private int hight;
    private Unit units[];
    private int unitCount;

    public Map(int width, int hight) {
        this.width = width;
        this.hight = hight;
        this.units = new Unit[width * hight];
        this.unitCount = 0;
    }

    public Map() {
        this(10, 10);
    }

    public void print() {
        for (int y = 0; y < this.hight; y++) {
            for (int x = 0; x < this.width; x++) {
                printBlock(x, y);
            }
            System.out.println();
        }
    }

    private void printBlock(int x, int y) {
        for (int i = 0; i < unitCount; i++) {
            Unit unit = this.units[i];
            if (unit.isOn(x, y)) {
                System.out.print(unit.getSymbol());
                return;
            }
        }
        System.out.print("-");
    }

    public String toString() { /// Overind method
        return "Map(" + this.width + "," + this.hight + ")";
    }

    public void add(Unit unit) {
        if (unitCount == (width * hight))
            return;
        this.units[unitCount] = unit;
        unitCount++;
    }

    public void printUnit() {
        for (int i = 0; i < unitCount; i++) {
            System.out.println(this.units[i]);
        }
    }

    public boolean isOn(int x, int y) {
        return isInWidth(x) && isInHight(y);
    }

    public boolean isInWidth(int x) {
        return x >= 0 && x < width;
    }

    public boolean isInHight(int y) {
        return y >= 0 && y < hight;
    }

    public boolean hasDominate(int x, int y) {
        for (int i = 0; i < unitCount; i++) {
            Unit unit = this.units[i];
            if (unit.isDominate() && (unit.isOn(x, y))) {
                return true;
            }
        }
        return false;
    }
}
